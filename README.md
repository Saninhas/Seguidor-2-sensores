# Robô Seguidor de Linha com 2 sensores
### Seguidor de Linha é uma modalidade de competição na qual o robô que completar mais rápido o trajeto marcado por uma linha branca vence. Posto isso, aqui foi desenvolvido um robô com 2 sensores para fins de aprendizado durante a capacitação da MinervaBots, equipe de robótica da UFRJ.
Se estiver interessado(a), clique [aqui](https://minervabots.poli.ufrj.br/links/) para mais detalhes da equipe de robótica MinervaBots.
